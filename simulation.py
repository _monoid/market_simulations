import random
from itertools import repeat
import matplotlib.pyplot as plt
import numpy as np

# Global static parameters
number_of_nodes = 100
number_of_infos = 100
info_start_price = 1.0
price_reduce_percantage = 0.01

# Loging variables
price_of_infos = [0.0] * number_of_infos
number_of_tokens = []


class Market:
    def __init__(self):
        self.asks = set()
        self.bids = set()
        self.price_history = []

    def fill(self, agents):
        for idx, agent in enumerate(agents):
            lowest_ask = agent.lowest_ask()
            if lowest_ask is not None:
                self.asks.add((lowest_ask, idx))
            highest_bid = agent.highest_bid()
            if highest_bid is not None:
                self.bids.add((highest_bid, idx))

    def resolve(self, agents):
        # TODO Butify
        order_mean_price = np.mean([order[0]
                                    for order in self.asks.union(self.bids)])
        self.price_history.append(order_mean_price)

        successfull_asks = set(
            filter(lambda ask: order_mean_price > ask[0], self.asks))
        successfull_bids = set(
            filter(lambda bid: order_mean_price < bid[0], self.bids))

        successfull_count = min([len(successfull_asks), len(successfull_bids)])

        # print(successfull_count)

        for idx, aks in enumerate(successfull_asks):
            if(idx < successfull_count):
                agent = agents[aks[1]]
                agent.tokens -= 1
                agent.fiat += order_mean_price
            else:
                break

        for idx, bid in enumerate(successfull_bids):
            if(idx < successfull_count):
                agent = agents[bid[1]]
                agent.tokens += 1
                agent.fiat -= order_mean_price
            else:
                break

    def reset(self):
        self.asks = set()
        self.bids = set()

    def __repr__(self):
        return "[Market] asks: " + str(self.asks) + "bids: " + str(self.bids)

    def token_exchange(self, seller, buyer, fiat, burn=0):
        if burn >= 0 and burn <= 1:
                # TODO: replace fixed token amount
            seller.remove_tokens(1)
            seller.add_fiat(fiat)

            buyer.add_tokens(1 * (1 - burn))
            buyer.remove_fiat(fiat)
        else:
            raise ValueError(
                "The Token  Burn " + str(burn) + " is not between 0 and 1."
            )


class Info:
    def __init__(self, id, price):
        self.price = price
        self.id = id

    def __repr__(self):
        return "[Price] id: " + str(self.id) + "price: " + str(self.price)

    def reduce_price(self, per):
        self.price -= self.price * per


class Action:
    def __init__(self, buyAmount, sellAmount, lowest_ask, highest_bid):
        self.buyAmount = buyAmount
        self.sellAmount = sellAmount
        self.lowest_ask = lowest_ask
        self.highest_bid = highest_bid


class Strategy:

    def __init__(self, market):
        self.market = market

    def set_info_price(self, info_price):
        raise NotImplementedError

    def get_action(self, owned_fiat, info_price=None):
        raise NotImplementedError


class InfoConsumer(Strategy):
    # TODO: Improve set infoPrice
    infoPrice = 0

    def set_info_price(self, info_price):
        self.infoPrice = info_price

    def get_action(self, owned_fiat, owned_token):
        print(market.price_history)
        if(market.price_history):
            highest_bid = min(owned_fiat, market.price_history[-1] * 2)
        else:
            highest_bid = None
        return Action(self.infoPrice, None, None, highest_bid)


class OldStrategy(Strategy):
    def set_info_price(self, info_price):
        pass

    def get_action(self, owned_fiat, owned_token):
        # TODO Adapt for decimal numbers, complexity
        if owned_token > 1:
            # Random between [0, * 10 / self.tokens]
            # return (10 / self.tokens) / 2 + random.random() * 10 / self.tokens
            lowest_ask = 10 / owned_token
        else:
            lowest_ask = None

        # TODO Adapt for decimal numbers, complexity
        if owned_fiat > 0:
            # Random between [0, * self.fiat / 10]
            highest_bid = (
                random.random() * owned_fiat / 10
            ) / 2 + random.random() * owned_fiat / 10
            # highest_bid = owned_fiat / 10
        else:
            highest_bid = None

        return Action(100, 100, lowest_ask, highest_bid)

# Strategy infoseller wants to cash out


class Agent:
    def __init__(self, tokens, fiat, need, info, strategy):
        self.tokens = tokens  # random.randint(0, 100)
        self.fiat = fiat  # random.randint(0, 20000)
        # TODO: Need is contra intuitive
        self.need = need
        self.infos = []
        self.strategy = strategy
        self.current_wanted_info = None

        # TODO: Implement sigular infos
        # Two kinds of predictions that an agent can have on how the token price will change
        # Adding infos to info array. 88 -> [0 - 87]
        for i in range(info):
            self.infos.append(Info(i, info_start_price))
        self.infos_needed = list(range(info, number_of_infos))

    def __repr__(self):
        return "[Agent] id: " + str(id(self))

    def remove_tokens(self, amount):
        if self.tokens - amount < 0:
            raise ValueError("The Token Balance of " +
                             str(self) + " is to small.")
        else:
            self.tokens -= amount

    def add_tokens(self, amount):
        self.tokens += amount

    def add_fiat(self, fiat):
        self.fiat += fiat

    def remove_fiat(self, fiat):
        if self.fiat - fiat < 0:
            raise ValueError("The Fiat Balance of " +
                             str(self) + " is to small.")
        else:
            self.fiat -= fiat

    def add_info(self, id, price):
        self.infos.append(Info(id, price))
        self.infos_needed.remove(id)

    def find_info(self, id):
        for info in self.infos:
            if info.id == id:
                return info
        else:
            return None

    # Returns price if agent has info otherwise None
    def find_price(self, id):
        info = self.find_info(id)
        if info is not None:
            return info.price
        else:
            return None

    def reduce_price_of_info(self, id, per):
        info = self.find_info(id)
        info.reduce_price(per)

    # agent and price are not stored, asumption that agens checks market again after token purchase
    def set_best_offer(self, agent, id, price):
        if agent:
            if price <= self.tokens:
                # TODO should the price be adapted?
                self.add_info(id, price)
                self.remove_tokens(price)
                agent.add_tokens(price)
                self.current_wanted_info = None
                self.strategy.set_info_price(0)
            else:
                self.strategy.set_info_price(price)
                self.current_wanted_info = id
        else:
            self.current_wanted_info = None

    def get_wanted_Info_Id(self):
        if self.current_wanted_info:
            return self.current_wanted_info
        else:
            return random.choice(self.infos_needed)

    def lowest_ask(self):
        # TODO dont double trigger get_action
        return self.strategy.get_action(
            owned_fiat=self.fiat, owned_token=self.tokens
        ).lowest_ask

    def highest_bid(self):
        # TODO dont double trigger get_action
        return self.strategy.get_action(
            owned_fiat=self.fiat, owned_token=self.tokens
        ).highest_bid

    # Token Price = 1 + 0.5 * (fiat - token) / 10


# Returns provider with best price for info with id, otherwise None
def find_best_provider(id, consumer):
    best_provider = None
    best_price = None
    not_best_providers = []
    for provider in agents:
        if consumer != provider:
            price = provider.find_price(id)
            if price is not None and (best_price is None or price < best_price):
                if best_provider is not None:
                    not_best_providers.append(best_provider)
                best_provider = provider
                best_price = price
            elif price is not None:
                not_best_providers.append(provider)
    return best_provider, best_price, not_best_providers


# Helper for price of infos
def division_by_zero(a, b):
    if b == 0:
        return 0
    else:
        return a / b


# Distributions


def ids_with_low_more_probable():
    # Binomial? Was für eine
    return int(random.random() * number_of_infos * random.random())
    # Verteilung ist hier sinnvoll


def ids_equaly_probable():
    return int(random.random() * number_of_infos)


class Ploter:
    plots = []
    axs = []
    Lns = []
    Overlay_Lns = []

    def __init__(self, plots, title):
        self.plots = plots
        self.axs = [0] * len(plots)
        self.Lns = [0] * len(plots)
        self.Overlay_Lns = [0] * len(plots)

        self.fig = plt.figure()

        for idx, plot in enumerate(plots):
            # Edit here for different layout
            self.axs[idx] = self.fig.add_subplot(len(plots), 1, idx + 1)
            self.Lns[idx] = self.axs[idx].plot([])[0]
            self.Overlay_Lns[idx] = self.axs[idx].plot([idx])
            if idx == 0:
                plt.title(title)
            plt.ylabel(plot.title)

        plt.tight_layout(h_pad=1.0)
        plt.ion()
        plt.show()

    def draw(self):
        for idx, plot in enumerate(self.plots):
            data_storage = plot.data_storage
            overlay = plot.overlay
            self.Lns[idx].set_ydata(data_storage)
            if data_storage:
                min_y = min(data_storage)
                max_y = max(data_storage)
            else:
                min_y = 0
                max_y = 0
            padding = (max_y - min_y) * 0.1
            if padding == 0:
                padding = 1
            self.axs[idx].set_ylim([min_y - padding, max_y + padding])
            # self.axs[idx].tick_params(axis='y', which='major', pad=10)
            self.Lns[idx].set_xdata(range(len(data_storage)))
            self.axs[idx].set_xlim([0, len(data_storage) + len(overlay[0])])

            # Overlay
            if overlay[0]:
                self.Overlay_Lns.set_ydata(overlay[0])

                if overlay[1] == "end":
                    xdata = range(
                        len(self.data_storage), len(
                            overlay[0]) + len(self.data_storage)
                    )
                self.Overlay_Lns.set_xdata(xdata)
        plt.pause(0.0001)


class Plot:
    data_storage = []
    overlay = ([], "")

    def __init__(self, title):
        self.title = title

    def update_with_array(self, data):
        self.data_storage = data

    def update_with_data_point(self, data_point):
        self.data_storage.append(data_point)


# Trackers
def track_price_of_infos():
    price_of_infos = [0.0] * number_of_infos
    cummulated_number_of_infos = [0] * number_of_infos
    for agent in agents:
        for info in agent.infos:
            price_of_infos[info.id] += info.price
            cummulated_number_of_infos[info.id] += 1

    price_of_infos[:] = [
        division_by_zero(x, cummulated_number_of_infos[idx])
        for idx, x in enumerate(price_of_infos)
    ]

    # TODO Best Practice?
    return price_of_infos


def track_occurence_of_infos():
    price_of_infos = [0.0] * number_of_infos
    cummulated_number_of_infos = [0] * number_of_infos
    for agent in agents:
        for info in agent.infos:
            price_of_infos[info.id] += info.price
            cummulated_number_of_infos[info.id] += 1

    # TODO Best Practice?
    return cummulated_number_of_infos


# *** Main Program ***
class Simulation:
    def __init__(self, market):
        self.market = market

    def setup_agents(self, info_distribution_method):
        for _ in repeat(None, int(number_of_nodes / 2)):
            # Sepeculative Agents
            agents.append(
                Agent(
                    tokens=1000,
                    fiat=0,
                    need=1,
                    info=0,
                    strategy=OldStrategy(self.market),
                )
            )
        for _ in repeat(None, int(number_of_nodes / 2)):
            agents.append(
                # Info buying Agents
                Agent(
                    tokens=0,
                    fiat=1000,
                    need=0,
                    info=info_distribution_method(),
                    strategy=(InfoConsumer(self.market)),
                )
            )

    def run(self, iterations=10000):
        self.setup_agents(ids_equaly_probable)
        info_occurence_plot = Plot("Info Distribution")
        price_of_infos_plot = Plot("Price of Infos")
        price_of_tokens_plot = Plot("Price of Tokens")
        circulating_tokens_plot = Plot("Circulating Tokens")
        ploter = Ploter(
            [info_occurence_plot, price_of_infos_plot,
                price_of_tokens_plot, circulating_tokens_plot],
            "Intresting Plots",
        )
        while iterations > 0:
            self.round(ids_with_low_more_probable)
            # Plot info prices
            price_of_infos_plot.update_with_array(track_price_of_infos())

            # Plot info amount
            info_occurence_plot.update_with_array(track_occurence_of_infos())

            # Plot Fiat -> Token Price History
            price_of_tokens_plot.update_with_array(self.market.price_history)

            # Plot Circulating tokens
            circulating_tokens_plot.update_with_array(number_of_tokens)

            ploter.draw()
            iterations -= 1

    def round(self, request_distribution_method):
        for agent in agents:
            if info_market:
                if agent.need < random.random():

                    requested_info_id = (
                        agent.get_wanted_Info_Id()
                    )  # request_distribution_method()
                    best_provider, best_price, not_best_providers = find_best_provider(
                        requested_info_id, agent
                    )

                    if best_provider is not None:
                        agent.set_best_offer(
                            best_provider, requested_info_id, best_price
                        )
                        for provider in not_best_providers:
                            provider.reduce_price_of_info(
                                requested_info_id, price_reduce_percantage
                            )
                    else:
                        agent.set_best_offer(None, requested_info_id, None)
        if speculation_market:
            self.market.fill(agents)
            self.market.resolve(agents)
            self.market.reset()

        # Logging
        number_of_tokens.append(0)
        price = 0
        price_count = 0
        for agent in agents:
            # print(number_of_tokens[-1])
            number_of_tokens[-1] += agent.tokens
            if len(agent.infos) > 20:
                price += agent.infos[20].price
                price_count += 1


# Simulation variables
info_market = True
speculation_market = True
agents = []
iteration = 0
market = Market()
sim = Simulation(market)
sim.run()
